var structComponentHolder =
[
    [ "ComponentHolder", "structComponentHolder.html#acf89f29354f82c4ecc85d2c99e1e61ad", null ],
    [ "~ComponentHolder", "structComponentHolder.html#a78bc30fdf34d01e807aff90bef8ae4e4", null ],
    [ "compare", "structComponentHolder.html#a3548f5a7ed9756ed6a2d1ed72f17e68c", null ],
    [ "getBaseObj", "structComponentHolder.html#a15908e458c94a6d1457f562c0b4deb15", null ],
    [ "getComp", "structComponentHolder.html#a5e9263600389bd60259c5a429316f4da", null ],
    [ "getID", "structComponentHolder.html#ab7e3cf97757508fd4856bf242b0e1216", null ],
    [ "getName", "structComponentHolder.html#a36dbcd168f640cb09a2191a9b3a30bfe", null ],
    [ "getSubComp", "structComponentHolder.html#a71d796972864bf04edeb43a83340cdf3", null ],
    [ "name", "structComponentHolder.html#a0f63443cf5a29a2d4b1d9f2f9ab9b1ce", null ],
    [ "pobj", "structComponentHolder.html#ab59fa7ad8404ccab49cd01ea107f9ef9", null ]
];