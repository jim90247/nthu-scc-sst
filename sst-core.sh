#!/bin/bash
set -xe

export MPICC=mpicc MPICXX=mpicxx
export SST_CORE_HOME=$PKGS_ROOT/sst-core-9.0.0+impi

mkdir build
cd build

CFLAGS="-O3" CXXFLAGS="-O3" ../configure \
    --prefix=$SST_CORE_HOME \
    --with-zoltan=$PKGS_ROOT/zoltan-3.83+impi

make all -j
make install
